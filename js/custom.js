/*
 * Kopatheme
 * Released under the MIT License.
 */

 'use strict';

/**
 *  Variables & Functions
 * -------------------------------------------------------------------
 */

var variable = {
    'var_1':{
        'contact': {
            'address': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
            'marker': '/url image'
        },
        'validate': {
            'form': {
                'SUBMIT': 'Submit',
                'SENDING': 'Sending...'
            },
            'name': {
                'REQUIRED': 'Please enter your name',
                'MINLENGTH': 'At least {0} characters required'
            },
            'email': {
                'REQUIRED': 'Please enter your email',
                'EMAIL': 'Please enter a valid email'
            },
            'url': {
                'REQUIRED': 'Please enter your url',
                'URL': 'Please enter a valid url'
            },
            'message': {
                'REQUIRED': 'Please enter a message',
                'MINLENGTH': 'At least {0} characters required'
            }
        },
        'tweets': {
            'failed': 'Sorry, twitter is currently unavailable for this user.',
            'loading': 'Loading tweets...'
        }
    }
};



/**
 * Code
 * -------------------------------------------------------------------
 */


jQuery(document).ready(function() {


/*_____________ Header _____________*/

    /*--- main-menu ---*/

    if(sfmmenu_ul.length){
        sfmmenu_ul.superfish({
            speed: 'fast',
            delay: '300'
        });

        /*--- sub-menu direction ---*/
        
        mmenu_sub_direction(mmenu_ul,mmenu_li,sfmmenu_ul);

    }

    /*--- search-box-1 ---*/

    var sj_search_1 = jQuery('.kopa-search-box-1');
    if(sj_search_1.length) {
        var sj_search_1_label = jQuery(this).find('.sj-dropdown-btn');
        sj_search_1_label.on('click',function() {
            sj_search_1.find('.search-text').focus();
        });
    }

    /*--- onePageNav ---*/

    var sj_opNav   = jQuery('.op-nav-menu'),
        sj_opNavli = sj_opNav.find('li');
    if(sj_opNav.length){
        jQuery(this).onePageNav({
            currentClass: 'current-menu-item'
        });
    }

    sj_opNavli.each(function(){
        jQuery(this).on('click', function(){
            jQuery('.mobile-menu').slideUp().removeClass('active');
            sj_opNavli.removeClass('current-menu-item');
            var sj_opNavli_index = jQuery(this).index();
            sj_opNavli.each(function(){
                if(jQuery(this).index() == sj_opNavli_index){
                    jQuery(this).addClass('current-menu-item');
                }
            })
        });
    });


/*_____________ Mobile Menu _____________*/   
    

    if(jQuery('.mobile-nav').length) {
        var sj_mb_nav = jQuery('.mobile-nav');
        sj_mb_nav.each(function(){
            var sj_mbn_pull = jQuery(this).children('span'),
                sj_mbn_ul   = jQuery(this).children('.mobile-menu');

            sj_mbn_pull.on('click',function () {
                sj_mbn_ul.slideToggle('normal').toggleClass('active');
            });
            jQuery('.mobile-menu').navgoco({
                accordion: true
            });
            jQuery('.caret').removeClass('caret');

            jQuery("html").mouseup(function (e){
                if (!sj_mb_nav.is(e.target) && sj_mb_nav.has(e.target).length === 0){
                    sj_mbn_ul.slideUp().removeClass('active');
                }
            });

        });
    }


/*_____________ Scroll top Top _____________*/   

 
    jQuery('.sj-scrollup').on('click',function(){
        jQuery("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });


/*_____________ Accordion _____________*/


    var i1 = 0,
        accordion_1 = jQuery('.kopa-accordion');

    if(accordion_1.length){    
        accordion_1.each(function() {
            var ct_acc_1 = jQuery(this).find('.panel-group');
            ct_acc_1.each(function() {
                var sj_liod_num = 'accordion' + i1,
                    sj_liod_num1 = '#' + sj_liod_num;

                jQuery(this).attr('id',sj_liod_num);
                jQuery(this).find('.panel-default').each(function() {
                    jQuery(this).find('.panel-title').children('a').attr('data-parent', sj_liod_num1);
                    jQuery(this).find('.panel-title').children('a').attr('href', sj_liod_num1 + + jQuery(this).index());
                    jQuery(this).find('.panel-collapse').attr('id', sj_liod_num1 + + jQuery(this).index());
                });
            });
            ++i1;

            var panel_titles = accordion_1.find('.panel-title a');
            panel_titles.addClass("collapsed");
            jQuery('.panel-heading.active').find(panel_titles).removeClass("collapsed");

            panel_titles.on('click',function(){

                jQuery(this).closest('.kopa-accordion').find('.panel-heading').removeClass('active');
                jQuery(this).closest('.kopa-accordion').find('.collapse').collapse('hide');
                jQuery(this).closest('.panel-heading').next().collapse('toggle');

                var pn_heading = jQuery(this).parents('.panel-heading');
                if (!pn_heading.hasClass('active')) {
                    pn_heading.addClass('active');
                } else {
                    pn_heading.removeClass('active');
                }
            });

        }); 
    }


/*_____________ Slider Pro _____________*/

    var pslider_1 = jQuery('.sn-module-slider-1');
    var psj_1 = jQuery('.slider-pro-1');
    if (pslider_1.length) {
        psj_1.sliderPro({
            width: 1366,
            height: 762,
            arrows: false,
            buttons: true,
            waitForLayers: false,
            autoplay: true,
            fadeOutPreviousSlide: true,
            autoScaleLayers: true,
            slideDistance: 0,
            autoplayDelay: 7000,
            init: function(){
               jQuery(".sn-module-slider-1 .loading").hide();    
               jQuery(".sn-module-slider-1 .slider-pro").show();   
            }
        });
    };

    var pslider_2 = jQuery('.sn-module-slider-2');
    var psj_2 = jQuery('.slider-pro-2');
    if (pslider_2.length) {
        psj_2.sliderPro({
            width: 1366,
            height: 980,
            forceSize: 'fullWidth',
            arrows: false,
            buttons: true,
            waitForLayers: false,
            autoplay: true,
            fadeOutPreviousSlide: true,
            autoScaleLayers: true,
            slideDistance: 0,
            autoplayDelay: 7000,
            init: function(){
               jQuery(".sn-module-slider-2 .loading").hide();    
               jQuery(".sn-module-slider-2 .slider-pro").show();   
            }
        });
    };

/*_____________ google maps _____________*/

    if (jQuery('.kopa-map').length > 0) {

        var id_map = jQuery('.kopa-map').attr('id');
        var lat = parseFloat(jQuery('.kopa-map').attr('data-latitude'));
        var lng = parseFloat(jQuery('.kopa-map').attr('data-longitude'));
        var place = jQuery('.kopa-map').attr('data-place');
        var iconBase = '';

        var map = new GMaps({
            el: '#'+id_map,
            lat: lat,
            lng: lng,
            zoom: 10,
            zoomControl : true,
            zoomControlOpt: {
              style : 'SMALL',
              position: 'TOP_RIGHT'
            },
            panControl : false,
            streetViewControl : false,
            mapTypeControl: false,
            overviewMapControl: false,
            scrollwheel: false,
            styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#e9e4e1"},{"visibility":"on"}]},{"featureType":"landscape","elementType":"labels.text","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"labels.text.fill","stylers":[{"color":"#797979"}]},{"featureType":"landscape","elementType":"labels.text.stroke","stylers":[{"color":"#e9e4e1"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#d8cdcd"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway.controlled_access","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway.controlled_access","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.station","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#f6f5f1"},{"visibility":"on"}]}]
        });
        map.addMarker({
            lat: lat,
            lng: lng,
            title: place,
            icon: iconBase 
        });

    }

/*_____________ Validate form _____________*/
    
    var ct_form_1 = jQuery('.ct-form-1');
    if(ct_form_1.length) {
        jQuery(ct_form_1).each(function(){
            jQuery(ct_form_1).validate({
                // Add requirements to each of the fields
                rules: {
                    name: {
                        required: true,
                        minlength: 10
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: true,
                        minlength: 15
                    }
                },
                // Specify what error messages to display
                // when the user does something horrid
                messages: {
                    name: {
                        required: "Please enter your name.",
                        minlength: jQuery.format("At least {0} characters required.")
                    },
                    email: {
                        required: "Please enter your email.",
                        email: "Please enter a valid email."
                    },
                    message: {
                        required: "Please enter a message.",
                        minlength: jQuery.format("At least {0} characters required.")
                    }
                }
            });
        });
    }

/*_____________ Owl Carousel _____________*/


    var owl1 = jQuery(".owl-carousel-1");
    if(owl1.length){
        owl1.each(function(){
            var o1_pag = jQuery(this).data('pagination'),
                o1_nav = jQuery(this).data('navigation'),
                o1_item = jQuery(this).data('item'),
                o1_item_dt = jQuery(this).data('item-desktop'),
                o1_item_tl = jQuery(this).data('item-tablet'),
                o1_item_tls = jQuery(this).data('item-tablet-small');

            jQuery(this).owlCarousel({
                items: o1_item,
                itemsDesktop: o1_item_dt,
                itemsTablet: o1_item_tl,
                itemsTabletSmall: o1_item_tls,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: false,
                pagination: o1_pag,
                navigation: o1_nav,
                addClassActive: true,
                afterAction: function(){
                    jQuery(".owl-1-show").removeClass("owl-1-show");
                    owl1.find(".active").eq(1).addClass("owl-1-show");
                },
                afterInit: function(){
                    jQuery('.ct-slider-2').find('.loading').hide();    
                }
            });
        });
    }

    var owl2 = jQuery(".owl-carousel-2");
    if(owl2.length){
        owl2.each(function(){
            var o2_pag = jQuery(this).data('pagination'),
                o2_nav = jQuery(this).data('navigation');

            jQuery(this).owlCarousel({
                singleItem: true,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: false,
                pagination: o2_pag,
                navigation: o2_nav
            });
        });
    }

    var owl3 = jQuery(".owl-carousel-3");
    if(owl3.length){
        owl3.each(function(){
            var o3_item = jQuery(this).data('item'),
                o3_item_dt = jQuery(this).data('item-desktop'),
                o3_item_tl = jQuery(this).data('item-tablet');

            jQuery(this).owlCarousel({
                items: o3_item,
                itemsDesktop: o3_item_dt,
                itemsTablet: o3_item_tl,
                slideSpeed: 600,
                navigationText: ["prev","next"],
                autoPlay: false,
                pagination: false,
                navigation: true
            });
        });
    }

    var owl4 = jQuery(".owl-carousel-4");
    if(owl4.length){
        owl4.each(function(){
            var o4_pag       = jQuery(this).data('pagination'),
                o4_nav       = jQuery(this).data('navigation'),
                o4_auto_play = jQuery(this).data('autoplay');

            jQuery(this).owlCarousel({
                singleItem: true,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: o4_auto_play,
                pagination: o4_pag,
                navigation: o4_nav,
                afterInit: function(){
                    jQuery(".ct-carousel-4 .loading").hide();    
                    owl4.find('.owl-pagination').children('.owl-page').each(function(){
                        jQuery(this).prepend(jQuery(this).index() + 1);
                    });
                }
            });
        });
    }

    var owl5 = jQuery(".owl-carousel-5");
    if(owl5.length){
        owl5.each(function(){
            var o5_pag       = jQuery(this).data('pagination'),
                o5_nav       = jQuery(this).data('navigation'),
                o5_auto_play = jQuery(this).data('autoplay');

            jQuery(this).owlCarousel({
                singleItem: true,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: o5_auto_play,
                pagination: o5_pag,
                navigation: o5_nav,
                afterInit: function(){
                    jQuery(".ct-carousel-5 .loading").hide();    
                    owl5.find('.owl-pagination').children('.owl-page').each(function(){
                        jQuery(this).prepend(jQuery(this).index() + 1);
                    });
                }
            });
        });
    }

    var owl6 = jQuery(".owl-carousel-6");
    if(owl6.length){
        owl6.each(function(){
            jQuery(this).owlCarousel({
                singleItem: true,
                slideSpeed: 600,
                navigationText: ["prev","next"],
                autoPlay: true,
                pagination: false,
                navigation: true,
                afterInit: function(){
                    jQuery('.ct-carousel-6').find('.loading').hide();    
                }
            });
        });
    }

    var owl7 = jQuery(".owl-carousel-7");
    if(owl7.length){
        owl7.each(function(){
            var o7_pag = jQuery(this).data('pagination'),
                o7_nav = jQuery(this).data('navigation'),
                o7_item = jQuery(this).data('item'),
                o7_item_dt = jQuery(this).data('item-desktop'),
                o7_item_tl = jQuery(this).data('item-tablet'),
                o7_item_tls = jQuery(this).data('item-tablet-small');

            jQuery(this).owlCarousel({
                items: o7_item,
                itemsDesktop: o7_item_dt,
                itemsTablet: o7_item_tl,
                itemsTabletSmall: o7_item_tls,
                slideSpeed: 600,
                navigationText: false,
                autoPlay: false,
                pagination: o7_pag,
                navigation: o7_nav,
                addClassActive: true,
                afterAction: function(){
                    jQuery(".owl-7-show").removeClass("owl-7-show");
                    owl7.find(".active").eq(1).addClass("owl-7-show");
                },
                afterInit: function(){
                    jQuery('.ct-slider-2').find('.loading').hide();    
                }
            });
        });
    }

    var owl8 = jQuery(".owl-carousel-8");
    if(owl8.length){
        owl8.each(function(){
            var o8_pag = jQuery(this).data('pagination'),
                o8_nav = jQuery(this).data('navigation'),
                o8_item = jQuery(this).data('item'),
                o8_item_dt = jQuery(this).data('item-desktop'),
                o8_item_tl = jQuery(this).data('item-tablet');

            jQuery(this).owlCarousel({
                items: o8_item,
                itemsDesktop: o8_item_dt,
                itemsTablet: o8_item_tl,
                slideSpeed: 600,
                navigationText: ["prev","next"],
                autoPlay: false,
                pagination: o8_pag,
                navigation: o8_nav
            });
        });
    }

/*_____________ Popup _____________*/


    var sj_pu_1 = jQuery('.sj-popup-1');
    sj_pu_1.find('.popup-vd-link').magnificPopup({
        iframe: {
            patterns: {
                vimeo: {
                    index: 'vimeo.com/',
                    id: '/',
                    src: 'http://player.vimeo.com/video/%id%?autoplay=1'
                },
            }
        },
        type: 'iframe'
    });

    var sj_pu_2 = jQuery('.sj-popup-2');
    if(sj_pu_2.length){
        sj_pu_2.each(function(){
            jQuery(this).magnificPopup({
                delegate: '.popup-link-2',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1]
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                }
            });
        });

    }

/*_____________ Grid Item _____________*/

    var filter_1 = jQuery('.sn-module-filter-1');

    if (jQuery(filter_1).length) {

        jQuery(filter_1).each(function(){

            var container_1 = jQuery(this).find('.masonry-container'),
                m_filters_1 = jQuery(this).find('.masonry-filter').children('a');
            var masonryOptions = {
                columnWidth: 1,
                itemSelector : '.ms-item-01.show'
            };
            container_1.imagesLoaded(function(){

                var m_grid = container_1.masonry( masonryOptions);

                m_filters_1.on('click',function(event){
                    event.preventDefault();
                    m_filters_1.removeClass('active');
                    jQuery(this).addClass('active');
                    var m_filter_val = jQuery(this).data('val');

                    container_1.find('.ms-item-01').each(function(){
                        var m_item_val = jQuery(this).data('val').toString().split(',');                
                        var a = m_item_val.indexOf(m_filter_val.toString()) > -1;

                        if (m_filter_val == "*") {
                            jQuery(this).removeClass('hide');
                            jQuery(this).addClass('show');
                        } else {
                            if ( a == true) {
                                jQuery(this).removeClass('hide');
                                jQuery(this).addClass('show');  
                            } else {
                                jQuery(this).removeClass('show');
                                jQuery(this).addClass('hide');
                            }
                        }                               
                    });
                    container_1.masonry('layout');

                });

            });

        });
    }

    var filter_2 = jQuery('.sn-module-filter-2');

    if (jQuery(filter_2).length) {

        jQuery(filter_2).each(function(){

            var container_2 = jQuery(this).find('.masonry-container'),
                m_filters_2 = jQuery(this).find('.masonry-filter').children('a');
            var masonryOptions = {
                columnWidth: 1,
                itemSelector : '.ms-item-02.show'
            };
            container_2.imagesLoaded(function(){

                var m_grid = container_2.masonry( masonryOptions);

                m_filters_2.on('click',function(event){
                    event.preventDefault();
                    m_filters_2.removeClass('active');
                    jQuery(this).addClass('active');
                    var m_filter_val = jQuery(this).data('val');

                    container_2.find('.ms-item-02').each(function(){
                        var m_item_val = jQuery(this).data('val').toString().split(',');                
                        var a = m_item_val.indexOf(m_filter_val.toString()) > -1;

                        if (m_filter_val == "*") {
                            jQuery(this).removeClass('hide');
                            jQuery(this).addClass('show');
                        } else {
                            if ( a == true) {
                                jQuery(this).removeClass('hide');
                                jQuery(this).addClass('show');  
                            } else {
                                jQuery(this).removeClass('show');
                                jQuery(this).addClass('hide');
                            }
                        }                               
                    });
                    container_2.masonry('layout');

                });

            });

        });
    }

    var filter_3 = jQuery('.sn-module-filter-3');

    if (jQuery(filter_3).length) {

        jQuery(filter_3).each(function(){

            var container_3 = jQuery(this).find('.masonry-container'),
                m_filters_3 = jQuery(this).find('.masonry-filter').children('a');
            var masonryOptions = {
                columnWidth: 1,
                itemSelector : '.ms-item-03.show'
            };
            container_3.imagesLoaded(function(){

                var m_grid = container_3.masonry( masonryOptions);

                m_filters_3.on('click',function(event){
                    event.preventDefault();
                    m_filters_3.removeClass('active');
                    jQuery(this).addClass('active');
                    var m_filter_val = jQuery(this).data('val');

                    container_3.find('.ms-item-03').each(function(){
                        var m_item_val = jQuery(this).data('val').toString().split(',');                
                        var a = m_item_val.indexOf(m_filter_val.toString()) > -1;

                        if (m_filter_val == "*") {
                            jQuery(this).removeClass('hide');
                            jQuery(this).addClass('show');
                        } else {
                            if ( a == true) {
                                jQuery(this).removeClass('hide');
                                jQuery(this).addClass('show');  
                            } else {
                                jQuery(this).removeClass('show');
                                jQuery(this).addClass('hide');
                            }
                        }                               
                    });
                    container_3.masonry('layout');

                });

            });

        });
    }


/*_____________ Ajax Loadmore _____________*/

    var link_to_github_2 = "https://gist.githubusercontent.com/Stormie/7f43d6787a63059a806be6637b45c1a3/raw/dc0fd80595c35d32577f83aed585a46ca352e0cd/supernova-1";
    jQuery('.sj-lm-2').on('click',function(event){
        event.preventDefault();
        var lm_btn_2 = jQuery(this);
        jQuery.ajax({
            url:link_to_github_2,
            beforeSend: function( xhr ) {
            },
        })
        .done(function( data ) {

            jQuery(data).imagesLoaded(function() {

                var pos_ins_data_2 =  lm_btn_2.parent().prev();

                var items_2 = jQuery(data).find('.ms-item-02');
                if(items_2.length){
                    jQuery.each(items_2, function(items_2, index){
                        jQuery(pos_ins_data_2).append(jQuery(this)).masonry( 'appended', jQuery(this));
                    });
                } 
            });
        });
    });

    var link_to_github_3 = "https://gist.githubusercontent.com/Stormie/4526be583b2e41d6b2b7afcb8bef60f7/raw/ff41541ae87ecd7eb11547d2dd1131fb08965aba/supernova-2";
    jQuery('.sj-lm-3').on('click',function(event){
        event.preventDefault();
        var lm_btn_3 = jQuery(this);
        jQuery.ajax({
            url:link_to_github_3,
            beforeSend: function( xhr ) {
            },
        })
        .done(function( data ) {

            jQuery(data).imagesLoaded(function() {

                var pos_ins_data_3 =  lm_btn_3.parent().prev();

                var items_3 = jQuery(data).find('.ms-item-03');
                if(items_3.length){
                    jQuery.each(items_3, function(items_3, index){
                        jQuery(pos_ins_data_3).append(jQuery(this)).masonry( 'appended', jQuery(this));
                    });
                } 
            });
        });
    });



/*_____________ mCustomScrollbar _____________*/

    
    var sj_scroll = jQuery(".sj-scroll-horizontal");
    if(sj_scroll.length){

        sj_scroll.each(function(){
            sj_scroll.find('.sj-scroll-content').mCustomScrollbar({
                axis:"x",
                mouseWheel:{ enable: false },
                advanced:{
                    autoExpandHorizontalScroll:true
                }
            });
        })
    }

/*_____________ Validate form _____________*/
    
    var sj_form_1 = jQuery('.sj-form-1');
    if(sj_form_1.length) {
        jQuery(sj_form_1).each(function(){
            jQuery(sj_form_1).validate({
                // Add requirements to each of the fields
                rules: {
                    name: {
                        required: true,
                        minlength: 10
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: true,
                        minlength: 15
                    }
                },
                // Specify what error messages to display
                // when the user does something horrid
                messages: {
                    name: {
                        required: "Please enter your name.",
                        minlength: jQuery.format("At least {0} characters required.")
                    },
                    email: {
                        required: "Please enter your email.",
                        email: "Please enter a valid email."
                    },
                    message: {
                        required: "Please enter a message.",
                        minlength: jQuery.format("At least {0} characters required.")
                    }
                }
            });
        });
    }

    var sj_form_2 = jQuery('.sj-form-2');
    if(sj_form_2.length) {
        jQuery(sj_form_2).each(function(){
            jQuery(sj_form_2).validate({
                // Add requirements to each of the fields
                rules: {
                    name: {
                        required: true,
                        minlength: 10
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: true,
                        minlength: 15
                    }
                },
                // Specify what error messages to display
                // when the user does something horrid
                messages: {
                    name: {
                        required: "Please enter your name.",
                        minlength: jQuery.format("At least {0} characters required.")
                    },
                    email: {
                        required: "Please enter your email.",
                        email: "Please enter a valid email."
                    },
                    message: {
                        required: "Please enter a message.",
                        minlength: jQuery.format("At least {0} characters required.")
                    }
                }
            });
        });
    }


    var sj_form_3 = jQuery('.sj-form-3');
    if(sj_form_3.length) {
        jQuery(sj_form_3).each(function(){
            jQuery(sj_form_3).validate({
                // Add requirements to each of the fields
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 10
                    }
                },
                // Specify what error messages to display
                // when the user does something horrid
                messages: {
                    email: {
                        required: "Please enter your email",
                        email: "Please enter a valid email"
                    },
                    password: {
                        required: "Please enter your password",
                        minlength: jQuery.format("At least {0} characters required.")
                    }
                }
            });
        });
    }

    var sj_form_4 = jQuery('.sj-form-4');
    if(sj_form_4.length) {
        jQuery(sj_form_4).each(function(){
            jQuery(sj_form_4).validate({
                // Add requirements to each of the fields
                rules: {
                    name: {
                        required: true,
                        minlength: 10
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 10
                    },
                    repassword: {
                        required: true,
                        minlength: 10
                    }
                },
                // Specify what error messages to display
                // when the user does something horrid
                messages: {
                    name: {
                        required: "Please enter your name.",
                        minlength: jQuery.format("At least {0} characters required.")
                    },
                    email: {
                        required: "Please enter your email",
                        email: "Please enter a valid email"
                    },
                    password: {
                        required: "Please enter your password",
                        minlength: jQuery.format("At least {0} characters required.")
                    },
                    repassword: {
                        required: "Password not match",
                        minlength: jQuery.format("At least {0} characters required.")
                    }
                }
            });
        });
    }


/*_____________ Countdown _____________*/

    if (jQuery('.ct-countdown-1').length) {

        var ct_cd_1      = jQuery('.ct-countdown-1'),
            ct_cd_time_1 = ct_cd_1.data('year') + 
                            '/' + ct_cd_1.data('month') + 
                            '/' + ct_cd_1.data('day') + 
                            " " + ct_cd_1.data('time'),
            ct_cd_content_1 = ct_cd_1.find('ul');

        ct_cd_content_1.countdown(ct_cd_time_1,{elapse: true}).on('update.countdown', function(event) {
            if (event.elapsed) {
                jQuery(this).html(event.strftime(''
                +'<li><div><span>%D</span></div><p>days</p></li>' 
                +'<li><div><span>%H</span></div><p>hours</p></li>' 
                +'<li><div><span>%M</span></div><p>minutes</p></li>' 
                +'<li><div><span>%S</span></div><p>seconds</p></li>'));
            } else {
                jQuery(this).html(event.strftime(''
                +'<li><div><span>%D</span></div><p>days</p></li>' 
                +'<li><div><span>%H</span></div><p>hours</p></li>' 
                +'<li><div><span>%M</span></div><p>minutes</p></li>' 
                +'<li><div><span>%S</span></div><p>seconds</p></li>'));
            }
        });

    }

    if (jQuery('.ct-countdown-2').length) {

        function md_cd1_maxh(){
            var md_cd1_col = jQuery('.sn-module-countdown-1').find('.ct-row-1').children(),
                md_cd1_h   = 0;

            md_cd1_col.each(function(){
                var md_cd1_col_h = jQuery(this).height();
                if(md_cd1_col_h > md_cd1_h){
                    md_cd1_h = md_cd1_col_h;
                }
                return md_cd1_h;
            });
            md_cd1_col.last().children().height(md_cd1_h);
        }

        var ct_cd_2      = jQuery('.ct-countdown-2'),
            ct_cd_time_2 = ct_cd_2.data('year') + 
                            '/' + ct_cd_2.data('month') + 
                            '/' + ct_cd_2.data('day') + 
                            " " + ct_cd_2.data('time'),
            ct_cd_content_2 = ct_cd_2.find('ul');

        ct_cd_content_2.countdown(ct_cd_time_2,{elapse: true}).on('update.countdown', function(event) {
            if (event.elapsed) {
                jQuery(this).html(event.strftime(''
                +'<li><div><h5><span>%w</span><sup>weeks</sup></h5></div></li>'
                +'<li><div><h5><span>%d</span><sup>days</sup></h5></div></li>'
                +'<li><div><h5><span>%H</span><sup>hours</sup></h5></div></li>'
                +'<li><div><h5><span>%M</span><sup>minutes</sup></h5></div></li>'
                +'<li><div><h5><span>%S</span><sup>seconds</sup></h5></div></li>'));
                md_cd1_maxh();

            } else {
                jQuery(this).html(event.strftime(''
                +'<li><div><h5><span>%w</span><sup>weeks</sup></h5></div></li>'
                +'<li><div><h5><span>%d</span><sup>days</sup></h5></div></li>'
                +'<li><div><h5><span>%H</span><sup>hours</sup></h5></div></li>'
                +'<li><div><h5><span>%M</span><sup>minutes</sup></h5></div></li>'
                +'<li><div><h5><span>%S</span><sup>seconds</sup></h5></div></li>'));
                md_cd1_maxh();
            }
        });

    }




/*_____________ Product filter _____________*/

    var i2 = 0;
    if(jQuery('.ct-form-box-3').length){    
        jQuery('.ct-form-box-3').each(function() {
            var radio_item = jQuery(this).find('.ct-radio-box');
            radio_item.each(function() {
                var item_index_1 = 'rb' + i2 + jQuery(this).index();
                jQuery(this).find('input').attr("id",item_index_1);
                jQuery(this).find('label').attr("for",item_index_1);
            });
            ++i2;
        }); 
    }

// /*_____________ Background Parallax _____________*/

// if (jQuery(window).width() > 992) {

//     var sj_bg_parallax = jQuery(".kopa-area-parallax");
//     if (sj_bg_parallax.length) {
//         sj_bg_parallax.parallax("50%", 0.3);
//     }

// }


/*_____________ Scroll Animation _____________*/


    
    if(jQuery('.wow').length){
        var wow = new WOW({mobile: false,offset: 10});
        wow.init();
    }


/*_____________ Custom _____________*/


    /*--- dropdown ---*/

    var sj_ddn = jQuery('.sj-dropdown');
    if(sj_ddn.length) {
        jQuery(sj_ddn).each(function() {
            var sj_this        = jQuery(this),
                sj_ddn_btn     = sj_this.find('.sj-dropdown-btn'),
                sj_ddn_content = sj_this.find('.sj-dropdown-content');
            sj_ddn_btn.on('click', function(){
                sj_ddn_content.toggleClass('active');
            });
            jQuery("html").mouseup(function (e){
                if (!sj_this.is(e.target) && sj_this.has(e.target).length === 0){
                    sj_ddn_content.removeClass('active');
                }
            });
        });
    }

    /*--- get-value ---*/

    var sj_get_vl = jQuery('.sj-get-value');
    if(sj_get_vl.length){
        sj_get_vl.each(function(){
            var sj_vl      = jQuery(this).find('.sj-value'),
                sj_vl_list = jQuery(this).find('.sj-value-list').children();
            sj_vl_list.each(function(){
                jQuery(this).on('click',function(event){
                    event.preventDefault();
                    var sj_vl_content = jQuery(this).text();
                    sj_vl.text(sj_vl_content);
                    jQuery(this).closest('.sj-dropdown-content').removeClass('active');
                });
            });    
        })
    }


    /*--- order-list ---*/

    function sj_order_list(){
        var sj_ulod = jQuery('.kopa-order-list');
        if(sj_ulod.length){
            sj_ulod.each(function(){
                var sj_liod = jQuery(this).children('li');
                sj_liod.each(function() {
                    var sj_liod_num   = jQuery(this).index() + 1 + '.',
                        sj_liod_num_1 = '0' + (jQuery(this).index() + 1);
                    jQuery(this).find('.kopa-order-num').html(sj_liod_num);
                    jQuery(this).find('.kopa-order-num-1').html(sj_liod_num_1);
                });
            });
        }
    }
    sj_order_list();

    /*--- area-space ---*/

    sj_area_space();
    

    /*--- lettering ---*/

    var sj_ltr = jQuery('.sj-ltr');
    if(sj_ltr.length){
        sj_ltr.each(function(){
            jQuery(this).lettering().addClass('active');
        });
    }
    

    /*--- curve-text ---*/

    var sj_cvt = jQuery('.sj-cvt');
    if(sj_cvt.length){
        sj_cvt.each(function(){
            jQuery(this).circleType({fitText:true}).addClass('active');
        });
    }

    /*--- smooth scroll ---*/

    var sj_sm_scroll = jQuery('.sj-smooth-scroll');
    if(sj_sm_scroll.length){
        sj_sm_scroll.each(function(){
            var sj_sm_scroll_btn = sj_sm_scroll.find('a');
            sj_sm_scroll_btn.on('click',function(event) {
                event.preventDefault();
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 800);
                        return false;
                    }
                }
            });
        });
    }

    /*--- countup ---*/

    var sj_counterup = jQuery('.sj-counter');
    if(sj_counterup.length) {
        sj_counterup.each(function(){
            jQuery(this).counterUp({
                delay: 10,
                time: 1000
            });
        });
    }

    /*--- PieChart ---*/

    var sj_piechart = jQuery('.sj-piechart');
    if(sj_piechart.length){
        sj_piechart.each(function() {
            var color = jQuery(this).data('color');
            var width = jQuery(this).data('width');
            var size  = jQuery(this).data('size');
            var time  = jQuery(this).data('time');

            jQuery(this).easyPieChart({
                barColor: color,
                trackColor: '#e5e5e5',
                scaleColor: 'rgba(255,255,255, 0)',
                lineWidth: width,
                size: size,
                animate: time,
                onStep: function(from, to, percent) {
                    jQuery(this.el).find('.percent').text(Math.round(percent));
                }
            });
        });
    }

    var sj_piechart_2 = jQuery('.sj-piechart-2');
    if(sj_piechart_2.length){
        sj_piechart_2.each(function() {
            var color = jQuery(this).data('color');
            var width = jQuery(this).data('width');
            var size  = jQuery(this).data('size');
            var time  = jQuery(this).data('time');

            jQuery(this).easyPieChart({
                barColor: color,
                trackColor: '#e5e5e5',
                scaleColor: 'rgba(255,255,255, 0)',
                lineWidth: width,
                size: size,
                animate: time,
                onStep: function(from, to, percent) {
                    jQuery(this.el).closest('.ct-piechart-2').find('.percent').text(Math.round(percent));
                }
            });
        });
    }

    /*--- area-custom ---*/

    var h_window   = jQuery(window).height(),
        area_ct    = jQuery('.kopa-area-custom'),
        area_ct_1    = jQuery('.kopa-area-custom-1');
        
    if(area_ct.length) {
        area_ct.css('min-height',h_window);
    }
    if(area_ct_1.length) {
        var act1_h = h_window - 300;
        area_ct_1.css('min-height',act1_h);
    }

    /*---custom ---*/


    

/*_____________ Woocommerce _____________*/

    if(jQuery('.wc-prettyphoto').length){
        jQuery('.wc-prettyphoto').find("a[data-rel ^='prettyPhoto']").prettyPhoto(); 
    }

    if(jQuery('.woocommerce-tabs').length) {
        var ct_li_3 = jQuery('.wc-tabs').find('li');
        ct_li_3.on('click', function(event){
            event.preventDefault();
            jQuery(this).closest('.wc-tabs').find('li').removeClass('active');
            jQuery(this).addClass('active');
        });
    }

    jQuery('.description_tab').on('click', function(){
        jQuery('#tab-description').addClass('active');
        jQuery('#tab-reviews').removeClass('active');
    });

    jQuery('.reviews_tab').on('click', function(){
        jQuery('#tab-description').removeClass('active');
        jQuery('#tab-reviews').addClass('active');
    });

    var md_woo_pf = jQuery('.widget_price_filter');
    if(md_woo_pf.length){

        var uislider_1 = md_woo_pf.find('.slider-range-1'),
            uimin_1    = md_woo_pf.find('.from'),
            uimax_1    = md_woo_pf.find('.to');

        uislider_1.slider({
            range: true,
            min: 0,
            max: 69,
            values: [ 9, 69 ],
            slide: function( event, ui ) {
                uimin_1.text("$" + ui.values[ 0 ]);
                uimax_1.text("$" + ui.values[ 1 ]);
            }
        });

        uimin_1.text( "$" + uislider_1.slider( "values", 0 ));
        uimax_1.text( "$" + uislider_1.slider( "values", 1 ));
    }

    var rm_item_1 = jQuery('.rm-item');
    if(rm_item_1.length) {
        jQuery(rm_item_1).each(function() {
            var rm_btn_1 = jQuery(this).find('.remove');
            rm_btn_1.on('click', function(event){
                event.preventDefault();
                jQuery(this).closest(rm_item_1).remove();
            });
        });
    }



/*_____________ MatchHeight _____________*/

    
    if(jQuery('.ul-mh').length){
        mh_1();
    }


});

function mh_1(){
    jQuery('.ul-mh').children().matchHeight();
}
function sj_area_space(){
    var w_w  = jQuery(window).width(), 
        ct_w = jQuery('.container').width();
           
    if(jQuery('.container').length == 0){
        ct_w = 1170;
    }

    var sp_w = (w_w - ct_w - 120)/2,
        pl_w = jQuery('.sj-plw'),
        pr_w = jQuery('.sj-prw');

    pl_w.css({
        "padding-left": sp_w
    });
    pr_w.css({
        "padding-right": sp_w
    });
}

var mmenu_ul   = jQuery('.main-nav').find('.main-menu'),
    mmenu_li   = mmenu_ul.children('li'),
    sfmmenu_ul = jQuery('.main-nav').find('.sf-menu');
function menu_rtl(mli_child, mli_osl, mlic_w){
    mli_child.each(function(){
        var mlic_num  = jQuery(this).parents('li').length - 1,
            mlic_osl  = mli_osl + (mlic_w * mlic_num),
            mlic_osr  = jQuery(window).width() - (mlic_osl);
        if(mlic_w > mlic_osr){
            jQuery(this).addClass('rtl');
        }
    });
}
function mmenu_sub_direction(mmenu_ul,mmenu_li,sfmmenu_ul){
    mmenu_li.each(function(){
        var mli_osl   = jQuery(this).offset().left,
            mli_osr   = jQuery(window).width() - (mli_osl + jQuery(this).outerWidth()),
            mli_child = jQuery(this).find('ul'),
            mlic_w    = mli_child.outerWidth();

        menu_rtl(mli_child, mli_osl, mlic_w);

        var mlic_rtl = jQuery('.rtl');
        mlic_rtl.each(function(){   
            var mlic_rtl_num  = jQuery(this).parents('.rtl').length + 1,
                mlic_rtl_osr  = mli_osr + (mlic_w * mlic_rtl_num),
                mlic_rtl_osl  = jQuery(window).width() - (mlic_rtl_osr);
            if(mlic_rtl_osl < 0){
                jQuery(this).removeClass('rtl');
            }
        });

    });
}

jQuery(window).resize(function(){
    sj_area_space();
    mmenu_sub_direction(mmenu_ul,mmenu_li,sfmmenu_ul);
});